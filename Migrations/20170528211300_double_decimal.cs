﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shipping.Migrations
{
    public partial class double_decimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_Siginon_Bulk_Type_Siginon_Bulk_TypeId",
                table: "Shipment");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_Siginon_Bulk_TypeId",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "timestamp",
                table: "Siginon_FEU_Type");

            migrationBuilder.DropColumn(
                name: "AgencyFee",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Bond",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "CFS",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "ConventionalBagging",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "CustomsDocumentation",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "DeliveryOrder",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Expedition",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "KPAHigh",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "KPALow",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Kephis",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "MssLevy",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Quayside",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Rdl",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "RebaggingAgent",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "RebaggingWfp",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Reconstitution",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "ShuntIn",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "ShuntOut",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Stevedoring",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Storage",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Tagging",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "WarehouseIn",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "WarehouseOut",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "WarehouseWfpIn",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "WarehouseWfpOut",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Wharfage",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Siginon_Bulk_TypeId",
                table: "Shipment");

            migrationBuilder.RenameColumn(
                name: "WharfageFee",
                table: "Siginon_Bulk_Type",
                newName: "ExpendituresLabel");

            migrationBuilder.AddColumn<decimal>(
                name: "ContractualAmountPayable",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "FinalAmountPayable",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "InitialAmountPayable",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ShipmentId",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Standard",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Tonnage",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "UsdMt",
                table: "Siginon_Bulk_Type",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Tagging",
                table: "Siginon_Bulk",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "KPALow",
                table: "Siginon_Bulk",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractualAmountPayable",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "FinalAmountPayable",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "InitialAmountPayable",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "ShipmentId",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Standard",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "Tonnage",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "UsdMt",
                table: "Siginon_Bulk_Type");

            migrationBuilder.RenameColumn(
                name: "ExpendituresLabel",
                table: "Siginon_Bulk_Type",
                newName: "WharfageFee");

            migrationBuilder.AddColumn<DateTime>(
                name: "timestamp",
                table: "Siginon_FEU_Type",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "AgencyFee",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bond",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CFS",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ConventionalBagging",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomsDocumentation",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeliveryOrder",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Expedition",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KPAHigh",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KPALow",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Kephis",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MssLevy",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Quayside",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Rdl",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RebaggingAgent",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RebaggingWfp",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reconstitution",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShuntIn",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShuntOut",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Stevedoring",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Storage",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tagging",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WarehouseIn",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WarehouseOut",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WarehouseWfpIn",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WarehouseWfpOut",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Wharfage",
                table: "Siginon_Bulk_Type",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "Tagging",
                table: "Siginon_Bulk",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<double>(
                name: "KPALow",
                table: "Siginon_Bulk",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<int>(
                name: "Siginon_Bulk_TypeId",
                table: "Shipment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_Siginon_Bulk_TypeId",
                table: "Shipment",
                column: "Siginon_Bulk_TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_Siginon_Bulk_Type_Siginon_Bulk_TypeId",
                table: "Shipment",
                column: "Siginon_Bulk_TypeId",
                principalTable: "Siginon_Bulk_Type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
