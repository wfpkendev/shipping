using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;
using Shipping.Models;

namespace Shipping.Controllers
{
    public class Siginon_BulkController : Controller
    {
        private readonly CargoContext _context;

        public Siginon_BulkController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Siginon_Bulk
        public async Task<IActionResult> Index()
        {
            return View(await _context.SiginonBulkSettings.ToListAsync());
        }

        // GET: Siginon_Bulk/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                id = 2;
                //return NotFound();
            }

            var siginon_Bulk = await _context.SiginonBulkSettings
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk == null)
            {
                return NotFound();
            }

            return View(siginon_Bulk);
        }

        // GET: Siginon_Bulk/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Siginon_Bulk/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,timestamp,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,CFS,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,Expedition,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Siginon_Bulk siginon_Bulk)
        {
            if (ModelState.IsValid)
            {
                _context.Add(siginon_Bulk);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(siginon_Bulk);
        }

        // GET: Siginon_Bulk/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_Bulk = await _context.SiginonBulkSettings.SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk == null)
            {
                return NotFound();
            }
            return View(siginon_Bulk);
        }

        // POST: Siginon_Bulk/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,timestamp,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,CFS,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,Expedition,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Siginon_Bulk siginon_Bulk)
        {
            if (id != siginon_Bulk.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(siginon_Bulk);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Siginon_BulkExists(siginon_Bulk.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details");
            }
            return View(siginon_Bulk);
        }

        // GET: Siginon_Bulk/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_Bulk = await _context.SiginonBulkSettings
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk == null)
            {
                return NotFound();
            }

            return View(siginon_Bulk);
        }

        // POST: Siginon_Bulk/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var siginon_Bulk = await _context.SiginonBulkSettings.SingleOrDefaultAsync(m => m.Id == id);
            _context.SiginonBulkSettings.Remove(siginon_Bulk);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Siginon_BulkExists(int id)
        {
            return _context.SiginonBulkSettings.Any(e => e.Id == id);
        }
    }
}
