﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shipping.Migrations
{
    public partial class ModifySiginonBulk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ConventionalBagging",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DeliveryOrder",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Expedition",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Kephis",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MssLevy",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Quayside",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Rdl",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RebaggingAgent",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RebaggingWfp",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Reconstitution",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Stevedoring",
                table: "Siginon_Bulk",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Tagging",
                table: "Siginon_Bulk",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConventionalBagging",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "DeliveryOrder",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Expedition",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Kephis",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "MssLevy",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Quayside",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Rdl",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "RebaggingAgent",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "RebaggingWfp",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Reconstitution",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Stevedoring",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "Tagging",
                table: "Siginon_Bulk");

            migrationBuilder.DropColumn(
                name: "TransportQuaysideGbhl",
                table: "Siginon_Bulk");
        }
    }
}
