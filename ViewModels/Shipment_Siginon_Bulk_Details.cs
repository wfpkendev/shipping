﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shipping.ViewModels
{
    public class Shipment_Siginon_Bulk_Details
    {
        public int ShipmentId;

        public decimal? Rate_Mt { get; set; }
        public decimal ContractualAmount { get; set; }
        public decimal InitialInvoice { get; set; }
        public decimal FinalInvoice { get; set; }
        public decimal ActualMt { get; set; }

        [DisplayName("KPA Charges 0-500MTG")]
        public string KPALow { get; set; }


        [DisplayName("KPA Charges >500MTG")]
        public string KPAHigh { get; set; }

        [DisplayName("Shunting Inside Port")]
        public string ShuntIn { get; set; }

        [DisplayName("Shunting Outside Port(Kipevu CFS/Agency W'hs")]
        public string ShuntOut { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: In")]
        public string WarehouseIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: Out")]
        public string WarehouseOut { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Bagged: In")]
        public string WarehouseWfpIn { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Bagged: Out")]
        public string WarehouseWfpOut { get; set; }

        [DisplayName("Storage per week (minimum 4 weeks free period)")]
        public string Storage { get; set; }

        [DisplayName("Customs Documentation")]
        public string CustomsDocumentation { get; set; }

        [DisplayName("Wharfage, empty bags")]
        public string Wharfage { get; set; }

        [DisplayName("CFS Charges (Kipevu/others where applicable")]
        public string CFS { get; set; }

        [DisplayName("Wharfage Fee")]
        public string WharfageFee { get; set; }

        [DisplayName("Agency Fee")]
        public string AgencyFee { get; set; }

        [DisplayName("Bond")]
        public string Bond { get; set; }

        [DisplayName("Conventional Bagging (including KPA)")]
        public string ConventionalBagging { get; set; }

        [DisplayName("KEPHIS")]
        public string Kephis { get; set; }

        [DisplayName("Delivery order (BOL) fees")]
        public string DeliveryOrder { get; set; }

        [DisplayName("Stevedoring")]
        public string Stevedoring { get; set; }

        [DisplayName("Expedition charges if required")]
        public string Expedition { get; set; }

        [DisplayName("Reconstitution on received damaged cargo")]
        public string Reconstitution { get; set; }

        [DisplayName("Bagging at Quayside")]
        public string Quayside { get; set; }

        [DisplayName("Rebagging at agent's warehouse")]
        public string RebaggingAgent { get; set; }

        [DisplayName("Rebagging at WFP provided warehouse")]
        public string RebaggingWfp { get; set; }

        [DisplayName("MSS Levy")]
        public string MssLevy { get; set; }

        [DisplayName("RDL")]
        public string Rdl { get; set; }

        [DisplayName("Local Transport of Empty Bags from Quayside to GBHL")]
        public string TransportQuaysideGbhl { get; set; }

        [DisplayName("Tagging Services**")]
        public string Tagging { get; set; }
    }
}
