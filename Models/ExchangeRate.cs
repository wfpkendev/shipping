﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shipping.Models
{
    public class ExchangeRate
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public decimal Rate { get; set; }
        
    }
}
