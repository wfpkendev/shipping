using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;
using System.Reflection;

namespace Shipping.Models
{
    public class Siginon_Bulk_TypeController : Controller
    {
        private readonly CargoContext _context;

        public Siginon_Bulk_TypeController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Siginon_Bulk_Type
        public async Task<IActionResult> Index()
        {
            return View(await _context.Siginon_Bulk_Type.ToListAsync());
        }

        // GET: Siginon_Bulk_Type/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_Bulk_Type = await _context.Siginon_Bulk_Type
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk_Type == null)
            {
                return NotFound();
            }

            return View(siginon_Bulk_Type);
        }

        // GET: Siginon_Bulk_Type/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Siginon_Bulk_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,CFS,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,Expedition,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Siginon_Bulk_Type siginon_Bulk_Type)
        {
            if (ModelState.IsValid)
            {
                _context.Add(siginon_Bulk_Type);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(siginon_Bulk_Type);
        }

        // GET: Siginon_Bulk_Type/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_Bulk_Type = await _context.Siginon_Bulk_Type.SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk_Type == null)
            {
                return NotFound();
            }
            return View(siginon_Bulk_Type);
        }

        // POST: Siginon_Bulk_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,CFS,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,Expedition,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Siginon_Bulk_Type siginon_Bulk_Type)
        {
            if (id != siginon_Bulk_Type.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(siginon_Bulk_Type);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Siginon_Bulk_TypeExists(siginon_Bulk_Type.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(siginon_Bulk_Type);
        }

        // GET: Siginon_Bulk_Type/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_Bulk_Type = await _context.Siginon_Bulk_Type
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_Bulk_Type == null)
            {
                return NotFound();
            }

            return View(siginon_Bulk_Type);
        }

        // POST: Siginon_Bulk_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var siginon_Bulk_Type = await _context.Siginon_Bulk_Type.SingleOrDefaultAsync(m => m.Id == id);
            _context.Siginon_Bulk_Type.Remove(siginon_Bulk_Type);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // POST: Siginon_Bulk_Type/Save/5
        [HttpPost, ActionName("Save")]

        public bool Save(string modifiedId, string modifiedCap, string modifiedIap, string modifiedFap, string modifiedTon, bool conversionFlag )
        {
            if ((modifiedCap != "NaN") && (modifiedCap != null))
            {
                var latestRate = 0.00M;
                var amountPayable = 0.00M;
                var siginon_Bulk_Type = _context.Siginon_Bulk_Type.SingleOrDefault(m => m.Id == Convert.ToInt32(modifiedId));
                if (conversionFlag == true)
                {

                    latestRate = getLatestExchangeRate();
                    amountPayable = Convert.ToDecimal(modifiedCap) / latestRate;
                    siginon_Bulk_Type.ContractualAmountPayable = amountPayable;
                    siginon_Bulk_Type.InitialAmountPayable = amountPayable;
                }

                else
                {
                    siginon_Bulk_Type.ContractualAmountPayable = Convert.ToDecimal(modifiedCap);
                    siginon_Bulk_Type.InitialAmountPayable = Convert.ToDecimal(modifiedIap);
                }
                                
                siginon_Bulk_Type.FinalAmountPayable = Convert.ToDecimal(modifiedFap);
                siginon_Bulk_Type.Tonnage = Convert.ToDecimal(modifiedTon);


                _context.Update(siginon_Bulk_Type);
                _context.SaveChangesAsync();
                return true;
            }
            else { return false; }
        }

        private decimal getLatestExchangeRate()
        {
            var exchangeRate = _context.ExchangeRate.OrderByDescending(x => x.Id).First();
            return exchangeRate.Rate;
        }

        private bool Siginon_Bulk_TypeExists(int id)
        {
            return _context.Siginon_Bulk_Type.Any(e => e.Id == id);
        }
    }
}
