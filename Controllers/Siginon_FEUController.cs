using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;
using Shipping.Models;

namespace Shipping.Controllers
{
    public class Siginon_FEUController : Controller
    {
        private readonly CargoContext _context;

        public Siginon_FEUController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Siginon_FEU
        public async Task<IActionResult> Index()
        {
            return View(await _context.SiginonFEUSettings.ToListAsync());
        }

        // GET: Siginon_FEU/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                id = 1;
                //return NotFound();
            }

            var siginon_FEU = await _context.SiginonFEUSettings
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU == null)
            {
                return NotFound();
            }

            return View(siginon_FEU);
        }

        // GET: Siginon_FEU/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Siginon_FEU/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,timestamp,KPA,ShuntIn,ShuntOut,Destuffing,DestuffingWFP,Ramp,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,StorageBagged,StorageCartonized,NFI,CustomsDocumentation,Bond,Wharfage,AgencyFee,TrackingFee,AlterationFee,CFS,Cleaning,LiftInsidePort,LiftOutsidePort,DeliveryOrder,DropOff,Terminal,Isps,KpaStorage,KpaRemarshalling,EctsCharges,Mss,Equipment,ReconstitutionBagged,ReconstitutionCartonized,Rebagging,BaggedAgentWarehouse,CartonizedAgentWarehouse")] Siginon_FEU siginon_FEU)
        {
            if (ModelState.IsValid)
            {
                _context.Add(siginon_FEU);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(siginon_FEU);
        }

        // GET: Siginon_FEU/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_FEU = await _context.SiginonFEUSettings.SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU == null)
            {
                return NotFound();
            }
            return View(siginon_FEU);
        }

        // POST: Siginon_FEU/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,timestamp,KPA,ShuntIn,ShuntOut,Destuffing,DestuffingWFP,Ramp,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,StorageBagged,StorageCartonized,NFI,CustomsDocumentation,Bond,Wharfage,AgencyFee,TrackingFee,AlterationFee,CFS,Cleaning,LiftInsidePort,LiftOutsidePort,DeliveryOrder,DropOff,Terminal,Isps,KpaStorage,KpaRemarshalling,EctsCharges,Mss,Equipment,ReconstitutionBagged,ReconstitutionCartonized,Rebagging,BaggedAgentWarehouse,CartonizedAgentWarehouse")] Siginon_FEU siginon_FEU)
        {
            if (id != siginon_FEU.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(siginon_FEU);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Siginon_FEUExists(siginon_FEU.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details");
            }
            return View(siginon_FEU);
        }

        // GET: Siginon_FEU/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_FEU = await _context.SiginonFEUSettings
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU == null)
            {
                return NotFound();
            }

            return View(siginon_FEU);
        }

        // POST: Siginon_FEU/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var siginon_FEU = await _context.SiginonFEUSettings.SingleOrDefaultAsync(m => m.Id == id);
            _context.SiginonFEUSettings.Remove(siginon_FEU);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Siginon_FEUExists(int id)
        {
            return _context.SiginonFEUSettings.Any(e => e.Id == id);
        }
    }
}
