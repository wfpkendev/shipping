using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;
using Shipping.Models;

namespace Shipping.Controllers
{
    public class ShipmentsController : Controller
    {
        private readonly CargoContext _context;

        public ShipmentsController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Shipments
        public IActionResult Index(int? id)
        {
            var vendor = "";
            var shipments =  _context.Shipments
                  .Where(m => m.VendorId == id).ToList();

            if (id == 1)
            {
                vendor = "Damco";
            }
            else
            {
                vendor = "Bollore";
            }
            ViewBag.VendorId = id;
            ViewBag.Vendor = vendor;

            return View(shipments);
        }

        // GET: Shipments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shipment = await _context.Shipments
                .SingleOrDefaultAsync(m => m.ShipmentId == id);
            shipment.shipmentDetails = _context.Siginon_Bulk_Type
                .Where(m => m.ShipmentId == shipment.ShipmentId).ToList();
            if (shipment == null)
            {
                return NotFound();
            }

            return View(shipment);
            //return View("_PartialBulk");
        }

        // GET: Shipments/Create
        public IActionResult Create(int id)
        {
            var vendor = "";
            if (id == 1)
            {
                vendor = "Damco";
            }
            else
            {
                vendor = "Bollore";
            }
            ViewBag.VendorId = id;
            ViewBag.Vendor = vendor;
            return View();
        }

        public void AddShipmentDetails(Shipment shipment)
        {
            
            switch (shipment.VendorId)
            {
                case 1:
                    switch (shipment.Type)
                    {
                        case HowPacked.Damco_Bulk:
                            var shipmentBulkSettings = _context.SiginonBulkSettings.FirstOrDefault();
                            var shipmentDetails = new List<Siginon_Bulk_Type>{
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "KPA Charges 0-500MTG",
                                    UsdMt = shipmentBulkSettings.KPALow,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "KPA Charges >500MTG",
                                    UsdMt = shipmentBulkSettings.KPAHigh,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Shunting Inside Port",
                                    UsdMt = shipmentBulkSettings.ShuntIn,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Tonnage = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Shunting Outside Port(Kipevu CFS/Agency W'hs",
                                    UsdMt = shipmentBulkSettings.ShuntOut,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Tonnage = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Warehouse Handling at agent's warehouse - Bagged: In",
                                    UsdMt = shipmentBulkSettings.WarehouseIn,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Warehouse Handling at agent's warehouse - Bagged: Out",
                                    UsdMt = shipmentBulkSettings.WarehouseOut,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Warehouse Handling at WFP warehouse - Bagged: In",
                                    UsdMt = shipmentBulkSettings.WarehouseWfpIn,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Warehouse Handling at WFP warehouse - Bagged: Out",
                                    UsdMt = shipmentBulkSettings.WarehouseWfpOut,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Storage per week (minimum 4 weeks free period)",
                                    UsdMt = shipmentBulkSettings.WarehouseWfpOut,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Customs Documentation",
                                    UsdMt = shipmentBulkSettings.CustomsDocumentation,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Wharfage, empty bags",
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "CFS Charges (Kipevu/others where applicable",
                                    UsdMt = shipmentBulkSettings.CFS,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Wharfage Fee",
                                    UsdMt = shipmentBulkSettings.WharfageFee,
                                    InitialAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Agency Fee",
                                    UsdMt = shipmentBulkSettings.AgencyFee,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Bond",
                                    UsdMt = shipmentBulkSettings.Bond,
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = true},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Conventional Bagging (including KPA)",
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "KEPHIS",
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Delivery order (BOL) fees",
                                    UsdMt = shipmentBulkSettings.DeliveryOrder,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Stevedoring",
                                    UsdMt = shipmentBulkSettings.Stevedoring,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Expedition charges if required",
                                    UsdMt = shipmentBulkSettings.Expedition,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Reconstitution on received damaged cargo",
                                    UsdMt = shipmentBulkSettings.Reconstitution,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Bagging at Quayside",
                                    UsdMt = shipmentBulkSettings.Quayside,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Rebagging at agent's warehouse",
                                    UsdMt = shipmentBulkSettings.RebaggingAgent,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Rebagging at WFP provided warehouse",
                                    UsdMt = shipmentBulkSettings.RebaggingWfp,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "MSS Levy",
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "RDL",
                                    ContractualAmountPayable = 0.00M,
                                    FinalAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Local Transport of Empty Bags from Quayside to GBHL",
                                    UsdMt = shipmentBulkSettings.TransportQuaysideGbhl,
                                    ContractualAmountPayable = 0.00M,
                                    InitialAmountPayable = 0.00M,
                                    Standard = false},
                                new Siginon_Bulk_Type{
                                    ShipmentId = shipment.ShipmentId,
                                    ExpendituresLabel = "Tagging Services**",
                                    Standard = false}
                                };
                            _context.AddRange(shipmentDetails);
                            _context.SaveChanges();
                            break;
                        case HowPacked.Damco_FEU:
                            break;
                        case HowPacked.Damco_TEU:
                            break;
                    }
                    break;
                case 2:
                    break;
            }
        }



        // POST: Shipments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("VendorId,ShipmentId,BLNumber,SINumber,Commodity,Type,FinalDestination,Project,BILTonnage,LandedMt,ETA,FileRef,VesselName")] Shipment shipment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shipment);
                AddShipmentDetails(shipment);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", new { id = shipment.VendorId });
            }
            return View(shipment);
        }

        // GET: Shipments/Edit/5
        public async Task<IActionResult> Edit(int? id, int? vendorid)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shipment = await _context.Shipments.SingleOrDefaultAsync(m => m.ShipmentId == id);
            if (shipment == null)
            {
                return NotFound();
            }
            ViewBag.VendorId = vendorid;
            return View(shipment);
        }

        // POST: Shipments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ShipmentId,BLNumber,SINumber,Commodity,Type,FinalDestination,Project,BILTonnage,LandedMt,ETA,FileRef,VesselName,VendorId")] Shipment shipment)
        {
            if (id != shipment.ShipmentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shipment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShipmentExists(shipment.ShipmentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", new { id = shipment.VendorId });
               
            }
            return View(shipment);
        }

        // GET: Shipments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shipment = await _context.Shipments
                .SingleOrDefaultAsync(m => m.ShipmentId == id);
            if (shipment == null)
            {
                return NotFound();
            }

            return View(shipment);
        }

        // POST: Shipments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int? vendorid)
        {
            var shipment = await _context.Shipments.SingleOrDefaultAsync(m => m.ShipmentId == id);
            _context.Shipments.Remove(shipment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { id = vendorid });
        }

        private bool ShipmentExists(int id)
        {
            return _context.Shipments.Any(e => e.ShipmentId == id);
        }
    }
}
