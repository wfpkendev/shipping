﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shipping.Models;
using Microsoft.EntityFrameworkCore;

namespace Shipping.Data
{
    public class CargoContext : DbContext
    {
        public CargoContext(DbContextOptions<CargoContext> options) : base(options)
        {
        }

        public DbSet<CargoType> CargoTypes { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Siginon_Bulk> SiginonBulkSettings { get; set; }
        public DbSet<Bollore_Bulk> BolloreBulkSettings { get; set; }

        public DbSet<Siginon_FEU> SiginonFEUSettings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CargoType>().ToTable("CargoType");
            modelBuilder.Entity<Shipment>().ToTable("Shipment");
            modelBuilder.Entity<Siginon_Bulk>().ToTable("Siginon_Bulk");
            modelBuilder.Entity<Siginon_FEU>().ToTable("Siginon_FEU");
            modelBuilder.Entity<Bollore_Bulk>().ToTable("Bollore_Bulk");
            //modelBuilder.Entity<Bollore_FEU>().ToTable("Siginon_FEU");
        }

        public DbSet<Siginon_Bulk_Type> Siginon_Bulk_Type { get; set; }

        //public DbSet<Bollore_Bulk_Type> Bollore_Bulk_Type { get; set; }

        public DbSet<Siginon_FEU_Type> Siginon_FEU_Type { get; set; }

        public DbSet<Shipping.Models.ExchangeRate> ExchangeRate { get; set; }

        public DbSet<Shipping.Models.Bollore_Bulk> Bollore_Bulk { get; set; }
    }
}
