using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;
using Shipping.Models;

namespace Shipping.Controllers
{
    public class Bollore_BulkController : Controller
    {
        private readonly CargoContext _context;

        public Bollore_BulkController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Bollore_Bulk
        public async Task<IActionResult> Index()
        {
            return View(await _context.Bollore_Bulk.ToListAsync());
        }

        // GET: Bollore_Bulk/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                id = 2;
            }

            var bollore_Bulk = await _context.Bollore_Bulk
                .SingleOrDefaultAsync(m => m.Id == id);
            if (bollore_Bulk == null)
            {
                return NotFound();
            }

            return View(bollore_Bulk);
        }

        // GET: Bollore_Bulk/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bollore_Bulk/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,timestamp,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,BillOfLading,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Bollore_Bulk bollore_Bulk)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bollore_Bulk);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bollore_Bulk);
        }

        // GET: Bollore_Bulk/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bollore_Bulk = await _context.Bollore_Bulk.SingleOrDefaultAsync(m => m.Id == id);
            if (bollore_Bulk == null)
            {
                return NotFound();
            }
            return View(bollore_Bulk);
        }

        // POST: Bollore_Bulk/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,timestamp,KPALow,KPAHigh,ShuntIn,ShuntOut,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,Storage,CustomsDocumentation,Wharfage,WharfageFee,AgencyFee,Bond,ConventionalBagging,Kephis,DeliveryOrder,Stevedoring,BillOfLading,Reconstitution,Quayside,RebaggingAgent,RebaggingWfp,MssLevy,Rdl,TransportQuaysideGbhl,Tagging")] Bollore_Bulk bollore_Bulk)
        {
            if (id != bollore_Bulk.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bollore_Bulk);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Bollore_BulkExists(bollore_Bulk.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details");
            }
            return View(bollore_Bulk);
        }

        // GET: Bollore_Bulk/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bollore_Bulk = await _context.Bollore_Bulk
                .SingleOrDefaultAsync(m => m.Id == id);
            if (bollore_Bulk == null)
            {
                return NotFound();
            }

            return View(bollore_Bulk);
        }

        // POST: Bollore_Bulk/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bollore_Bulk = await _context.Bollore_Bulk.SingleOrDefaultAsync(m => m.Id == id);
            _context.Bollore_Bulk.Remove(bollore_Bulk);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Bollore_BulkExists(int id)
        {
            return _context.Bollore_Bulk.Any(e => e.Id == id);
        }
    }
}
