﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shipping.Models
{
    public class Siginon_FEU
    {
        public int Id { get; set; }
        
        public DateTime timestamp { get; set; }

        [DisplayName("KPA Charges")]
        public double KPA { get; set; }

        [DisplayName("Shunting Bagged: Inside Port")]
        public decimal ShuntIn { get; set; }

        [DisplayName("Shunting Outside Port(Kipevu CFS/Agency W'hs")]
        public decimal ShuntOut { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr")]
        public decimal Destuffing { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr at WFP warehouse")]
        public decimal DestuffingWFP { get; set; }

        [DisplayName("Hire of ramp to destuff vehicles/hour")]
        public decimal Ramp { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: In")]
        public decimal WarehouseIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: Out")]
        public decimal WarehouseOut { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Cartonized/Palletized: In")]
        public decimal WarehouseWfpIn { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Cartonized/Palletized: Out")]
        public decimal WarehouseWfpOut { get; set; }

        [DisplayName("Storage per week - bagged")]
        public decimal StorageBagged { get; set; }

        [DisplayName("Storage per week - cartonized")]
        public decimal StorageCartonized { get; set; }

        [DisplayName("NFI per CBM")]
        public decimal NFI { get; set; }

        [DisplayName("Customs Documentation")]
        public decimal CustomsDocumentation { get; set; }

        [DisplayName("Bond")]
        public decimal Bond { get; set; }

        [DisplayName("Wharfage Fee")]
        public decimal Wharfage { get; set; }

        [DisplayName("Agency Fee")]
        public decimal AgencyFee { get; set; }

        [DisplayName("Electronic Tracking Fee")]
        public decimal TrackingFee { get; set; }

        [DisplayName("Alteration Fee")]
        public decimal AlterationFee { get; set; }

        [DisplayName("CFS Handling")]
        public decimal CFS { get; set; }

        [DisplayName("Container cleaning charges")]
        public decimal Cleaning { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - inside port")]
        public decimal LiftInsidePort { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - outside port")]
        public decimal LiftOutsidePort { get; set; }

        [DisplayName("Delivery order (BOL) fee/destination documentation")]
        public decimal DeliveryOrder { get; set; }

        [DisplayName("Drop off charges")]
        public decimal DropOff { get; set; }

        [DisplayName("Terminal handling charge")]
        public decimal Terminal { get; set; }

        [DisplayName("ISPS or Import Service")]
        public decimal Isps { get; set; }

        [DisplayName("KPA storage charges (after expiry of free time)")]
        public decimal KpaStorage { get; set; }

        [DisplayName("KPA remarshalling")]
        public decimal KpaRemarshalling { get; set; }

        [DisplayName("ECTS charges")]
        public decimal EctsCharges { get; set; }

        [DisplayName("MSS Levy (customs code - 745)")]
        public decimal Mss { get; set; }

        [DisplayName("Equipment Management Fee")]
        public decimal Equipment { get; set; }

        [DisplayName("Reconstitution/Rebagging on received damaged cargo at agent warehouse - Bagged")]
        public decimal ReconstitutionBagged { get; set; }

        [DisplayName("Reconstitution/Rebagging on received damaged cargo at agent warehouse - Cartonized")]
        public decimal ReconstitutionCartonized { get; set; }

        [DisplayName("Rebagging")]
        public decimal Rebagging { get; set; }

        [DisplayName("Restacking of bagged cargo at agent warehouse")]
        public decimal BaggedAgentWarehouse { get; set; }

        [DisplayName("Restacking of cartonized/palletized cargo & jerrycans at agent warehouse")]
        public decimal CartonizedAgentWarehouse { get; set; }

        public ICollection<Shipment> Shipments { get; set; }
    }
}
