﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Shipping.Models
{
    public class Siginon_Bulk_Type
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key()]
        public int Id { get; set; }

        public int ShipmentId {get; set;}

        public string ExpendituresLabel { get; set; }

        public decimal UsdMt { get; set; }

        public decimal ContractualAmountPayable { get; set; }

        public decimal InitialAmountPayable { get; set; }

        public decimal FinalAmountPayable { get; set; }

        public decimal Tonnage { get; set; }

        public bool Standard { get; set; }
    }
}

