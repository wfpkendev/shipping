﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shipping.Models
{
    public class Siginon_FEU_Type
    {
        public int Id { get; set; }

       
        [DisplayName("KPA Charges")]
        public string KPA { get; set; }

        [DisplayName("Shunting Bagged: Inside Port")]
        public string ShuntIn { get; set; }

        [DisplayName("Shunting Outside Port(Kipevu CFS/Agency W'hs")]
        public string ShuntOut { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr")]
        public string Destuffing { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr at WFP warehouse")]
        public string DestuffingWFP { get; set; }

        [DisplayName("Hire of ramp to destuff vehicles/hour")]
        public string Ramp { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: In")]
        public string WarehouseIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: Out")]
        public string WarehouseOut { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Cartonized/Palletized: In")]
        public string WarehouseWfpIn { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Cartonized/Palletized: Out")]
        public string WarehouseWfpOut { get; set; }

        [DisplayName("Storage per week - bagged")]
        public string StorageBagged { get; set; }

        [DisplayName("Storage per week - cartonized")]
        public string StorageCartonized { get; set; }

        [DisplayName("NFI per CBM")]
        public string NFI { get; set; }

        [DisplayName("Customs Documentation")]
        public string CustomsDocumentation { get; set; }

        [DisplayName("Bond")]
        public string Bond { get; set; }

        [DisplayName("Wharfage Fee")]
        public string Wharfage { get; set; }

        [DisplayName("Agency Fee")]
        public string AgencyFee { get; set; }

        [DisplayName("Electronic Tracking Fee")]
        public string TrackingFee { get; set; }

        [DisplayName("Alteration Fee")]
        public string AlterationFee { get; set; }

        [DisplayName("CFS Handling")]
        public string CFS { get; set; }

        [DisplayName("Container cleaning charges")]
        public string Cleaning { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - inside port")]
        public string LiftInsidePort { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - outside port")]
        public string LiftOutsidePort { get; set; }

        [DisplayName("Delivery order (BOL) fee/destination documentation")]
        public string DeliveryOrder { get; set; }

        [DisplayName("Drop off charges")]
        public string DropOff { get; set; }

        [DisplayName("Terminal handling charge")]
        public string Terminal { get; set; }

        [DisplayName("ISPS or Import Service")]
        public string Isps { get; set; }

        [DisplayName("KPA storage charges (after expiry of free time)")]
        public string KpaStorage { get; set; }

        [DisplayName("KPA remarshalling")]
        public string KpaRemarshalling { get; set; }

        [DisplayName("ECTS charges")]
        public string EctsCharges { get; set; }

        [DisplayName("MSS Levy (customs code - 745)")]
        public string Mss { get; set; }

        [DisplayName("Equipment Management Fee")]
        public string Equipment { get; set; }

        [DisplayName("Reconstitution/Rebagging on received damaged cargo at agent warehouse - Bagged")]
        public string ReconstitutionBagged { get; set; }

        [DisplayName("Reconstitution/Rebagging on received damaged cargo at agent warehouse - Cartonized")]
        public string ReconstitutionCartonized { get; set; }

        [DisplayName("Rebagging")]
        public string Rebagging { get; set; }

        [DisplayName("Restacking of bagged cargo at agent warehouse")]
        public string BaggedAgentWarehouse { get; set; }

        [DisplayName("Restacking of cartonized/palletized cargo & jerrycans at agent warehouse")]
        public string CartonizedAgentWarehouse { get; set; }

        public ICollection<Shipment> Shipments { get; set; }
    }
}
