﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shipping.Migrations
{
    public partial class Siginon_Bulk_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Siginon_Bulk_TypeId",
                table: "Shipment",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Siginon_Bulk_Type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgencyFee = table.Column<string>(nullable: true),
                    Bond = table.Column<string>(nullable: true),
                    CFS = table.Column<string>(nullable: true),
                    ConventionalBagging = table.Column<string>(nullable: true),
                    CustomsDocumentation = table.Column<string>(nullable: true),
                    DeliveryOrder = table.Column<string>(nullable: true),
                    Expedition = table.Column<string>(nullable: true),
                    KPAHigh = table.Column<string>(nullable: true),
                    KPALow = table.Column<string>(nullable: true),
                    Kephis = table.Column<string>(nullable: true),
                    MssLevy = table.Column<string>(nullable: true),
                    Quayside = table.Column<string>(nullable: true),
                    Rdl = table.Column<string>(nullable: true),
                    RebaggingAgent = table.Column<string>(nullable: true),
                    RebaggingWfp = table.Column<string>(nullable: true),
                    Reconstitution = table.Column<string>(nullable: true),
                    ShuntIn = table.Column<string>(nullable: true),
                    ShuntOut = table.Column<string>(nullable: true),
                    Stevedoring = table.Column<string>(nullable: true),
                    Storage = table.Column<string>(nullable: true),
                    Tagging = table.Column<string>(nullable: true),
                    TransportQuaysideGbhl = table.Column<string>(nullable: true),
                    WarehouseIn = table.Column<string>(nullable: true),
                    WarehouseOut = table.Column<string>(nullable: true),
                    WarehouseWfpIn = table.Column<string>(nullable: true),
                    WarehouseWfpOut = table.Column<string>(nullable: true),
                    Wharfage = table.Column<string>(nullable: true),
                    WharfageFee = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siginon_Bulk_Type", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_Siginon_Bulk_TypeId",
                table: "Shipment",
                column: "Siginon_Bulk_TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_Siginon_Bulk_Type_Siginon_Bulk_TypeId",
                table: "Shipment",
                column: "Siginon_Bulk_TypeId",
                principalTable: "Siginon_Bulk_Type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_Siginon_Bulk_Type_Siginon_Bulk_TypeId",
                table: "Shipment");

            migrationBuilder.DropTable(
                name: "Siginon_Bulk_Type");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_Siginon_Bulk_TypeId",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "Siginon_Bulk_TypeId",
                table: "Shipment");
        }
    }
}
