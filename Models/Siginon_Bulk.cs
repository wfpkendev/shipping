﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using Shipping.Data;

namespace Shipping.Models
{
    public class Siginon_Bulk
    {
        public int Id { get; set; }
        
        public DateTime timestamp { get; set; }
        
        [DisplayName("KPA Charges 0-500MTG")]
        public decimal KPALow { get; set; }
       

        [DisplayName("KPA Charges >500MTG")]
        public decimal KPAHigh { get; set; }

        [DisplayName("Shunting Inside Port")]
        public decimal ShuntIn { get; set; }

        [DisplayName("Shunting Outside Port(Kipevu CFS/Agency W'hs")]
        public decimal ShuntOut { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: In")]
        public decimal WarehouseIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: Out")]
        public decimal WarehouseOut { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Bagged: In")]
        public decimal WarehouseWfpIn { get; set; }

        [DisplayName("Warehouse Handling at WFP warehouse - Bagged: Out")]
        public decimal WarehouseWfpOut { get; set; }

        [DisplayName("Storage per week (minimum 4 weeks free period)")]
        public decimal Storage { get; set; }

        [DisplayName("Customs Documentation")]
        public decimal CustomsDocumentation { get; set; }

        [DisplayName("Wharfage, empty bags")]
        public decimal Wharfage { get; set; }

        [DisplayName("CFS Charges (Kipevu/others where applicable")]
        public decimal CFS { get; set; }

        [DisplayName("Wharfage Fee")]
        public decimal WharfageFee { get; set; }

        [DisplayName("Agency Fee")]
        public decimal AgencyFee { get; set; }

        [DisplayName("Bond")]
        public decimal Bond { get; set; }

        [DisplayName("Conventional Bagging (including KPA)")]
        public decimal ConventionalBagging { get; set; }

        [DisplayName("KEPHIS")]
        public decimal Kephis { get; set; }

        [DisplayName("Delivery order (BOL) fees")]
        public decimal DeliveryOrder { get; set; }

        [DisplayName("Stevedoring")]
        public decimal Stevedoring { get; set; }

        [DisplayName("Expedition charges if required")]
        public decimal Expedition { get; set; }

        [DisplayName("Reconstitution on received damaged cargo")]
        public decimal Reconstitution { get; set; }

        [DisplayName("Bagging at Quayside")]
        public decimal Quayside { get; set; }

        [DisplayName("Rebagging at agent's warehouse")]
        public decimal RebaggingAgent { get; set; }

        [DisplayName("Rebagging at WFP provided warehouse")]
        public decimal RebaggingWfp { get; set; }

        [DisplayName("MSS Levy")]
        public decimal MssLevy { get; set; }

        [DisplayName("RDL")]
        public decimal Rdl { get; set; }

        [DisplayName("Local Transport of Empty Bags from Quayside to GBHL")]
        public decimal TransportQuaysideGbhl { get; set; }

        [DisplayName("Tagging Services**")]
        public decimal Tagging { get; set; }

        public ICollection<Shipment> Shipments { get; set; }

        
    }
}

