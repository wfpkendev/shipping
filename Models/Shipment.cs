﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Shipping.Models
{
    public enum HowPacked { Damco_Bulk, Damco_FEU, Damco_TEU, Bollore_Bulk, Bollore_FEU, Bollore_TEU }
    public class Shipment
    {
        public int VendorId { get; set; }
        public int ShipmentId { get; set; }

        [DisplayName("BL Number")]
        public string BLNumber { get; set; }

        [DisplayName("SI Number")]
        public string SINumber { get; set; }
        public string Commodity { get; set; }
        public HowPacked Type { get; set; }

        [DisplayName("Final Destination")]
        public string FinalDestination { get; set; }
        public string Project { get; set; }

        //[DisplayName("Exchange Rate")]
        //public int ExchangeRate { get; set; }

        [DisplayName("BIL Tonnage")]
        public decimal BILTonnage { get; set; }

        [DisplayName("Landed Mt")]
        public decimal LandedMt { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ETA { get; set; }

        [DisplayName("File Ref")]
        public string FileRef { get; set; }

        [DisplayName("Vessel Name")]
        public string VesselName { get; set; }

        public List<Siginon_Bulk_Type> shipmentDetails { get; set; }
    }
}
