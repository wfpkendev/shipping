using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shipping.Data;

namespace Shipping.Models
{
    public class Siginon_FEU_TypeController : Controller
    {
        private readonly CargoContext _context;

        public Siginon_FEU_TypeController(CargoContext context)
        {
            _context = context;    
        }

        // GET: Siginon_FEU_Type
        public async Task<IActionResult> Index()
        {
            return View(await _context.Siginon_FEU_Type.ToListAsync());
        }

        // GET: Siginon_FEU_Type/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_FEU_Type = await _context.Siginon_FEU_Type
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU_Type == null)
            {
                return NotFound();
            }

            return View(siginon_FEU_Type);
        }

        // GET: Siginon_FEU_Type/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Siginon_FEU_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,timestamp,KPA,ShuntIn,ShuntOut,Destuffing,DestuffingWFP,Ramp,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,StorageBagged,StorageCartonized,NFI,CustomsDocumentation,Bond,Wharfage,AgencyFee,TrackingFee,AlterationFee,CFS,Cleaning,LiftInsidePort,LiftOutsidePort,DeliveryOrder,DropOff,Terminal,Isps,KpaStorage,KpaRemarshalling,EctsCharges,Mss,Equipment,ReconstitutionBagged,ReconstitutionCartonized,Rebagging,BaggedAgentWarehouse,CartonizedAgentWarehouse")] Siginon_FEU_Type siginon_FEU_Type)
        {
            if (ModelState.IsValid)
            {
                _context.Add(siginon_FEU_Type);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(siginon_FEU_Type);
        }

        // GET: Siginon_FEU_Type/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_FEU_Type = await _context.Siginon_FEU_Type.SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU_Type == null)
            {
                return NotFound();
            }
            return View(siginon_FEU_Type);
        }

        // POST: Siginon_FEU_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,timestamp,KPA,ShuntIn,ShuntOut,Destuffing,DestuffingWFP,Ramp,WarehouseIn,WarehouseOut,WarehouseWfpIn,WarehouseWfpOut,StorageBagged,StorageCartonized,NFI,CustomsDocumentation,Bond,Wharfage,AgencyFee,TrackingFee,AlterationFee,CFS,Cleaning,LiftInsidePort,LiftOutsidePort,DeliveryOrder,DropOff,Terminal,Isps,KpaStorage,KpaRemarshalling,EctsCharges,Mss,Equipment,ReconstitutionBagged,ReconstitutionCartonized,Rebagging,BaggedAgentWarehouse,CartonizedAgentWarehouse")] Siginon_FEU_Type siginon_FEU_Type)
        {
            if (id != siginon_FEU_Type.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(siginon_FEU_Type);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Siginon_FEU_TypeExists(siginon_FEU_Type.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(siginon_FEU_Type);
        }

        // GET: Siginon_FEU_Type/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siginon_FEU_Type = await _context.Siginon_FEU_Type
                .SingleOrDefaultAsync(m => m.Id == id);
            if (siginon_FEU_Type == null)
            {
                return NotFound();
            }

            return View(siginon_FEU_Type);
        }

        // POST: Siginon_FEU_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var siginon_FEU_Type = await _context.Siginon_FEU_Type.SingleOrDefaultAsync(m => m.Id == id);
            _context.Siginon_FEU_Type.Remove(siginon_FEU_Type);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Siginon_FEU_TypeExists(int id)
        {
            return _context.Siginon_FEU_Type.Any(e => e.Id == id);
        }
    }
}
