﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shipping.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CargoType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CargoType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Siginon_Bulk",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgencyFee = table.Column<decimal>(nullable: false),
                    Bond = table.Column<decimal>(nullable: false),
                    CFS = table.Column<decimal>(nullable: false),
                    CustomsDocumentation = table.Column<decimal>(nullable: false),
                    KPAHigh = table.Column<decimal>(nullable: false),
                    KPALow = table.Column<double>(nullable: false),
                    ShuntIn = table.Column<decimal>(nullable: false),
                    ShuntOut = table.Column<decimal>(nullable: false),
                    Storage = table.Column<decimal>(nullable: false),
                    WarehouseIn = table.Column<decimal>(nullable: false),
                    WarehouseOut = table.Column<decimal>(nullable: false),
                    WarehouseWfpIn = table.Column<decimal>(nullable: false),
                    WarehouseWfpOut = table.Column<decimal>(nullable: false),
                    Wharfage = table.Column<decimal>(nullable: false),
                    WharfageFee = table.Column<decimal>(nullable: false),
                    timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siginon_Bulk", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Siginon_FEU",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgencyFee = table.Column<decimal>(nullable: false),
                    AlterationFee = table.Column<decimal>(nullable: false),
                    BaggedAgentWarehouse = table.Column<decimal>(nullable: false),
                    Bond = table.Column<decimal>(nullable: false),
                    CFS = table.Column<decimal>(nullable: false),
                    CartonizedAgentWarehouse = table.Column<decimal>(nullable: false),
                    Cleaning = table.Column<decimal>(nullable: false),
                    CustomsDocumentation = table.Column<decimal>(nullable: false),
                    DeliveryOrder = table.Column<decimal>(nullable: false),
                    Destuffing = table.Column<decimal>(nullable: false),
                    DestuffingWFP = table.Column<decimal>(nullable: false),
                    DropOff = table.Column<decimal>(nullable: false),
                    EctsCharges = table.Column<decimal>(nullable: false),
                    Equipment = table.Column<decimal>(nullable: false),
                    Isps = table.Column<decimal>(nullable: false),
                    KPA = table.Column<double>(nullable: false),
                    KpaRemarshalling = table.Column<decimal>(nullable: false),
                    KpaStorage = table.Column<decimal>(nullable: false),
                    LiftInsidePort = table.Column<decimal>(nullable: false),
                    LiftOutsidePort = table.Column<decimal>(nullable: false),
                    Mss = table.Column<decimal>(nullable: false),
                    NFI = table.Column<decimal>(nullable: false),
                    Ramp = table.Column<decimal>(nullable: false),
                    Rebagging = table.Column<decimal>(nullable: false),
                    ReconstitutionBagged = table.Column<decimal>(nullable: false),
                    ReconstitutionCartonized = table.Column<decimal>(nullable: false),
                    ShuntIn = table.Column<decimal>(nullable: false),
                    ShuntOut = table.Column<decimal>(nullable: false),
                    StorageBagged = table.Column<decimal>(nullable: false),
                    StorageCartonized = table.Column<decimal>(nullable: false),
                    Terminal = table.Column<decimal>(nullable: false),
                    TrackingFee = table.Column<decimal>(nullable: false),
                    WarehouseIn = table.Column<decimal>(nullable: false),
                    WarehouseOut = table.Column<decimal>(nullable: false),
                    WarehouseWfpIn = table.Column<decimal>(nullable: false),
                    WarehouseWfpOut = table.Column<decimal>(nullable: false),
                    Wharfage = table.Column<decimal>(nullable: false),
                    timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siginon_FEU", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shipment",
                columns: table => new
                {
                    ShipmentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BILTonnage = table.Column<decimal>(nullable: false),
                    BLNumber = table.Column<string>(nullable: true),
                    Commodity = table.Column<string>(nullable: true),
                    ETA = table.Column<DateTime>(nullable: false),
                    FileRef = table.Column<string>(nullable: true),
                    FinalDestination = table.Column<string>(nullable: true),
                    LandedMt = table.Column<decimal>(nullable: false),
                    Project = table.Column<int>(nullable: false),
                    SINumber = table.Column<string>(nullable: true),
                    Siginon_BulkId = table.Column<int>(nullable: true),
                    Siginon_FEUId = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    TypeId = table.Column<string>(nullable: true),
                    VesselName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment", x => x.ShipmentId);
                    table.ForeignKey(
                        name: "FK_Shipment_Siginon_Bulk_Siginon_BulkId",
                        column: x => x.Siginon_BulkId,
                        principalTable: "Siginon_Bulk",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Shipment_Siginon_FEU_Siginon_FEUId",
                        column: x => x.Siginon_FEUId,
                        principalTable: "Siginon_FEU",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_Siginon_BulkId",
                table: "Shipment",
                column: "Siginon_BulkId");

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_Siginon_FEUId",
                table: "Shipment",
                column: "Siginon_FEUId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CargoType");

            migrationBuilder.DropTable(
                name: "Shipment");

            migrationBuilder.DropTable(
                name: "Siginon_Bulk");

            migrationBuilder.DropTable(
                name: "Siginon_FEU");
        }
    }
}
