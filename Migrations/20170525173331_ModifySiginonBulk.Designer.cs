﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Shipping.Data;
using Shipping.Models;

namespace Shipping.Migrations
{
    [DbContext(typeof(CargoContext))]
    [Migration("20170525173331_ModifySiginonBulk")]
    partial class ModifySiginonBulk
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Shipping.Models.CargoType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("CargoType");
                });

            modelBuilder.Entity("Shipping.Models.Shipment", b =>
                {
                    b.Property<int>("ShipmentId")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("BILTonnage");

                    b.Property<string>("BLNumber");

                    b.Property<string>("Commodity");

                    b.Property<DateTime>("ETA");

                    b.Property<string>("FileRef");

                    b.Property<string>("FinalDestination");

                    b.Property<decimal>("LandedMt");

                    b.Property<int>("Project");

                    b.Property<string>("SINumber");

                    b.Property<int?>("Siginon_BulkId");

                    b.Property<int?>("Siginon_FEUId");

                    b.Property<int>("Type");

                    b.Property<string>("TypeId");

                    b.Property<string>("VesselName");

                    b.HasKey("ShipmentId");

                    b.HasIndex("Siginon_BulkId");

                    b.HasIndex("Siginon_FEUId");

                    b.ToTable("Shipment");
                });

            modelBuilder.Entity("Shipping.Models.Siginon_Bulk", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AgencyFee");

                    b.Property<decimal>("Bond");

                    b.Property<decimal>("CFS");

                    b.Property<decimal>("ConventionalBagging");

                    b.Property<decimal>("CustomsDocumentation");

                    b.Property<decimal>("DeliveryOrder");

                    b.Property<decimal>("Expedition");

                    b.Property<decimal>("KPAHigh");

                    b.Property<double>("KPALow");

                    b.Property<decimal>("Kephis");

                    b.Property<decimal>("MssLevy");

                    b.Property<decimal>("Quayside");

                    b.Property<decimal>("Rdl");

                    b.Property<decimal>("RebaggingAgent");

                    b.Property<decimal>("RebaggingWfp");

                    b.Property<decimal>("Reconstitution");

                    b.Property<decimal>("ShuntIn");

                    b.Property<decimal>("ShuntOut");

                    b.Property<decimal>("Stevedoring");

                    b.Property<decimal>("Storage");

                    b.Property<string>("Tagging");

                    b.Property<string>("TransportQuaysideGbhl");

                    b.Property<decimal>("WarehouseIn");

                    b.Property<decimal>("WarehouseOut");

                    b.Property<decimal>("WarehouseWfpIn");

                    b.Property<decimal>("WarehouseWfpOut");

                    b.Property<decimal>("Wharfage");

                    b.Property<decimal>("WharfageFee");

                    b.Property<DateTime>("timestamp");

                    b.HasKey("Id");

                    b.ToTable("Siginon_Bulk");
                });

            modelBuilder.Entity("Shipping.Models.Siginon_FEU", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("AgencyFee");

                    b.Property<decimal>("AlterationFee");

                    b.Property<decimal>("BaggedAgentWarehouse");

                    b.Property<decimal>("Bond");

                    b.Property<decimal>("CFS");

                    b.Property<decimal>("CartonizedAgentWarehouse");

                    b.Property<decimal>("Cleaning");

                    b.Property<decimal>("CustomsDocumentation");

                    b.Property<decimal>("DeliveryOrder");

                    b.Property<decimal>("Destuffing");

                    b.Property<decimal>("DestuffingWFP");

                    b.Property<decimal>("DropOff");

                    b.Property<decimal>("EctsCharges");

                    b.Property<decimal>("Equipment");

                    b.Property<decimal>("Isps");

                    b.Property<double>("KPA");

                    b.Property<decimal>("KpaRemarshalling");

                    b.Property<decimal>("KpaStorage");

                    b.Property<decimal>("LiftInsidePort");

                    b.Property<decimal>("LiftOutsidePort");

                    b.Property<decimal>("Mss");

                    b.Property<decimal>("NFI");

                    b.Property<decimal>("Ramp");

                    b.Property<decimal>("Rebagging");

                    b.Property<decimal>("ReconstitutionBagged");

                    b.Property<decimal>("ReconstitutionCartonized");

                    b.Property<decimal>("ShuntIn");

                    b.Property<decimal>("ShuntOut");

                    b.Property<decimal>("StorageBagged");

                    b.Property<decimal>("StorageCartonized");

                    b.Property<decimal>("Terminal");

                    b.Property<decimal>("TrackingFee");

                    b.Property<decimal>("WarehouseIn");

                    b.Property<decimal>("WarehouseOut");

                    b.Property<decimal>("WarehouseWfpIn");

                    b.Property<decimal>("WarehouseWfpOut");

                    b.Property<decimal>("Wharfage");

                    b.Property<DateTime>("timestamp");

                    b.HasKey("Id");

                    b.ToTable("Siginon_FEU");
                });

            modelBuilder.Entity("Shipping.Models.Shipment", b =>
                {
                    b.HasOne("Shipping.Models.Siginon_Bulk")
                        .WithMany("Shipments")
                        .HasForeignKey("Siginon_BulkId");

                    b.HasOne("Shipping.Models.Siginon_FEU")
                        .WithMany("Shipments")
                        .HasForeignKey("Siginon_FEUId");
                });
        }
    }
}
