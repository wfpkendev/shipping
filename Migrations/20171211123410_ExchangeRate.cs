﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shipping.Migrations
{
    public partial class ExchangeRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExchangeRate",
                table: "Shipment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Siginon_Bulk_Type_ShipmentId",
                table: "Siginon_Bulk_Type",
                column: "ShipmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Siginon_Bulk_Type_Shipment_ShipmentId",
                table: "Siginon_Bulk_Type",
                column: "ShipmentId",
                principalTable: "Shipment",
                principalColumn: "ShipmentId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Siginon_Bulk_Type_Shipment_ShipmentId",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropIndex(
                name: "IX_Siginon_Bulk_Type_ShipmentId",
                table: "Siginon_Bulk_Type");

            migrationBuilder.DropColumn(
                name: "ExchangeRate",
                table: "Shipment");
        }
    }
}
