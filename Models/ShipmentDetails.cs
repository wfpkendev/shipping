﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shipping.Models
{
    public class ShipmentDetails
    {
        public int ShipmentId;

        public decimal? Rate_Mt { get; set; }
        public decimal? Rate_Feu { get; set; }
        public decimal? Rate_Teu { get; set; }
        public decimal ContractualAmount { get; set; }
        public decimal InitialInvoice { get; set; }
        public decimal FinalInvoice { get; set; }
        public decimal ActualMt { get; set; }
    }
}
