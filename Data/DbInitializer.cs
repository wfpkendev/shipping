﻿using Shipping.Models;
using System;
using System.Linq;

namespace Shipping.Data
{
    public static class DbInitializer
    {
        public static void Initialize(CargoContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.CargoTypes.Any())
            {
                return;   // DB has been seeded
            }

            var cargotypes = new CargoType[]
            {
            new CargoType{Name="Damco_Bulk"},
            new CargoType{Name="Damco_FEU"},
            new CargoType{Name="Damco_TEU"},
            new CargoType{Name="Bollore_TEU"},
            new CargoType{Name="Bollore_FEU"},
            new CargoType{Name="Bollore_Bulk"}
            
            };
            foreach (CargoType c in cargotypes)
            {
                context.CargoTypes.Add(c);
            }
            context.SaveChanges();

            
        }
    }
}