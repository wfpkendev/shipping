﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shipping.Models
{
    public class Siginon_TEU_Type
    {
        public int Id { get; set; }

        [DisplayName("KPA Charges")]
        public double KPA { get; set; }

        [DisplayName("Shunting Bagged: Inside Port")]
        public decimal ShuntIn { get; set; }

        [DisplayName("Shunting Outside Port(CFS/Agency W'hs")]
        public decimal ShuntOut { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr at agent's warehouse")]
        public decimal Destuffing { get; set; }

        [DisplayName("Destuffing of container carrying NFI using forklift per Hr at WFP warehouse")]
        public decimal DestuffingWFP { get; set; }

        [DisplayName("Hire of ramp to destuff vehicles/hour")]
        public decimal Ramp { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: In")]
        public decimal WarehouseIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Bagged: Out")]
        public decimal WarehouseOut { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Cartonized/Palletized: In")]
        public decimal WarehouseAgentIn { get; set; }

        [DisplayName("Warehouse Handling at agent's warehouse - Cartonized/Palletized: Out")]
        public decimal WarehouseAgentOut { get; set; }

        [DisplayName("Storage per week - bagged")]
        public decimal StorageBagged { get; set; }

        [DisplayName("Storage per week - cartonized")]
        public decimal StorageCartonized { get; set; }

        [DisplayName("NFI per CBM")]
        public decimal NFI { get; set; }

        [DisplayName("Customs Documentation")]
        public decimal CustomsDocumentation { get; set; }



        [DisplayName("Wharfage Fee")]
        public decimal Wharfage { get; set; }

        [DisplayName("Agency Commission")]
        public decimal AgencyCommission { get; set; }

        [DisplayName("Bond")]
        public decimal Bond { get; set; }



        [DisplayName("CFS Handling")]
        public decimal CFS { get; set; }

        [DisplayName("Container Handling Charges")]
        public decimal Cleaning { get; set; }

        [DisplayName("Import Service")]
        public decimal Import { get; set; }

        [DisplayName("Electronic Fee")]
        public decimal Electronic { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - inside port")]
        public decimal LiftInsidePort { get; set; }

        [DisplayName("Lift on/off for full and empty SOC - outside port")]
        public decimal LiftOutsidePort { get; set; }

        [DisplayName("Alteration Fees")]
        public decimal AlterationFees { get; set; }

        [DisplayName("Delivery order (BOL) fee")]
        public decimal DeliveryOrder { get; set; }

        [DisplayName("Drop Off Charges")]
        public decimal DropOff { get; set; }

        [DisplayName("Terminal handling charge")]
        public decimal Terminal { get; set; }

        [DisplayName("ISPS")]
        public decimal Isps { get; set; }

        [DisplayName("KPA storage charges (after expiry of free time)")]
        public decimal KpaStorage { get; set; }

        [DisplayName("KPA remarshalling")]
        public decimal KpaRemarshalling { get; set; }

        [DisplayName("Destination Documentation Fee")]
        public decimal DestinationDocumentation { get; set; }



        [DisplayName("MSS Levy (customs code - 745)")]
        public decimal Mss { get; set; }

        [DisplayName("RDL (customs code - 747)")]
        public decimal Rdl { get; set; }

        [DisplayName("Handling charges")]
        public decimal Handling { get; set; }

        [DisplayName("Equipment Management Fee")]
        public decimal Equipment { get; set; }


        [DisplayName("Reconstitution on received damaged cargo at agent warehouse - Cartonized")]
        public decimal ReconstitutionCartonized { get; set; }

        [DisplayName("Rebagging")]
        public decimal Rebagging { get; set; }

        [DisplayName("Restacking of bagged cargo at agent warehouse")]
        public decimal BaggedAgentWarehouse { get; set; }

        [DisplayName("Restacking of cartonized/palletized at agent warehouse")]
        public decimal CartonizedAgentWarehouse { get; set; }

        public ICollection<Shipment> Shipments { get; set; }
    }
}
