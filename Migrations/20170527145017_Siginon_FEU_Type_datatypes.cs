﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shipping.Migrations
{
    public partial class Siginon_FEU_Type_datatypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Siginon_FEU_TypeId",
                table: "Shipment",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Siginon_FEU_Type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgencyFee = table.Column<string>(nullable: true),
                    AlterationFee = table.Column<string>(nullable: true),
                    BaggedAgentWarehouse = table.Column<string>(nullable: true),
                    Bond = table.Column<string>(nullable: true),
                    CFS = table.Column<string>(nullable: true),
                    CartonizedAgentWarehouse = table.Column<string>(nullable: true),
                    Cleaning = table.Column<string>(nullable: true),
                    CustomsDocumentation = table.Column<string>(nullable: true),
                    DeliveryOrder = table.Column<string>(nullable: true),
                    Destuffing = table.Column<string>(nullable: true),
                    DestuffingWFP = table.Column<string>(nullable: true),
                    DropOff = table.Column<string>(nullable: true),
                    EctsCharges = table.Column<string>(nullable: true),
                    Equipment = table.Column<string>(nullable: true),
                    Isps = table.Column<string>(nullable: true),
                    KPA = table.Column<string>(nullable: true),
                    KpaRemarshalling = table.Column<string>(nullable: true),
                    KpaStorage = table.Column<string>(nullable: true),
                    LiftInsidePort = table.Column<string>(nullable: true),
                    LiftOutsidePort = table.Column<string>(nullable: true),
                    Mss = table.Column<string>(nullable: true),
                    NFI = table.Column<string>(nullable: true),
                    Ramp = table.Column<string>(nullable: true),
                    Rebagging = table.Column<string>(nullable: true),
                    ReconstitutionBagged = table.Column<string>(nullable: true),
                    ReconstitutionCartonized = table.Column<string>(nullable: true),
                    ShuntIn = table.Column<string>(nullable: true),
                    ShuntOut = table.Column<string>(nullable: true),
                    StorageBagged = table.Column<string>(nullable: true),
                    StorageCartonized = table.Column<string>(nullable: true),
                    Terminal = table.Column<string>(nullable: true),
                    TrackingFee = table.Column<string>(nullable: true),
                    WarehouseIn = table.Column<string>(nullable: true),
                    WarehouseOut = table.Column<string>(nullable: true),
                    WarehouseWfpIn = table.Column<string>(nullable: true),
                    WarehouseWfpOut = table.Column<string>(nullable: true),
                    Wharfage = table.Column<string>(nullable: true),
                    timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siginon_FEU_Type", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_Siginon_FEU_TypeId",
                table: "Shipment",
                column: "Siginon_FEU_TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_Siginon_FEU_Type_Siginon_FEU_TypeId",
                table: "Shipment",
                column: "Siginon_FEU_TypeId",
                principalTable: "Siginon_FEU_Type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_Siginon_FEU_Type_Siginon_FEU_TypeId",
                table: "Shipment");

            migrationBuilder.DropTable(
                name: "Siginon_FEU_Type");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_Siginon_FEU_TypeId",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "Siginon_FEU_TypeId",
                table: "Shipment");
        }
    }
}
